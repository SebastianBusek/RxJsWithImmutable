import {Component} from '@angular/core';
import {Observable} from "rxjs";
import {Store} from '@ngrx/store';
import {id} from '../utils/id';
import {
  AddPersonAction,
  RemovePersonAction,
  RemoveGuestAction,
  AddGuestAction,
  ToggleAttendingAction
} from '../actions/actions';
import 'rxjs/Rx';
import {partyModel} from "../selectors/party-model";
import {Person} from "../models/person";

@Component({
  selector: 'app-root',
  styles: [`
    .page-content {
      width: 800px;
      margin: 0 auto;
    }
    
    .demo-card-wide.mdl-card {
      width: 512px;
      margin: 0 auto;
    }
  `],
  template: `
    <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
      <header class="mdl-layout__header" style="margin-bottom: 16px;">
        <div class="mdl-layout__header-row">
          <span class="mdl-layout-title">Party planner</span>
        </div>
      </header>
      <main class="mdl-layout__content">
        <div class="page-content">
          <div class="demo-card-wide mdl-card mdl-shadow--2dp">
            <div class="mdl-card__supporting-text">
              <party-stats
                [invited]="(model | async)?.total"
                [attending]="(model | async)?.attending"
                [guests]="(model | async)?.guests">
              </party-stats>
            </div>
            <div class="mdl-card__actions mdl-card--border">
              <person-input (addPerson)="addPerson($event)"></person-input>
            </div>
          </div>
          <filter-select (updateFilter)="updateFilter($event)"></filter-select>
          <person-list
            [people]="(model | async)?.people"
            (addGuest)="addGuest($event)"
            (removeGuest)="removeGuest($event)"
            (removePerson)="removePerson($event)"
            (toggleAttending)="toggleAttending($event)">
          </person-list>
        </div>
      </main>
    </div>
  `
})
export class AppComponent {
  public model;

  constructor(private _store: Store<any>) {
    this.model = Observable.combineLatest(
      _store.select('people'),
      _store.select('partyFilter')).let(partyModel());
  }

  addPerson(name: string) {
    this._store.dispatch(new AddPersonAction(new Person(id(), name)));
  }

  addGuest(id) {
    this._store.dispatch(new AddGuestAction(id));
  }

  removeGuest(id) {
    this._store.dispatch(new RemoveGuestAction(id));
  }

  removePerson(id) {
    this._store.dispatch(new RemovePersonAction(id));
  }

  toggleAttending(id) {
    this._store.dispatch(new ToggleAttendingAction(id));
  }

  updateFilter(filter) {
    this._store.dispatch({type: filter});
  }
}
