import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import {PersonInput} from "./person-input/person-input.component";
import {PersonList} from "./person-list/person-list.component";
import {StoreModule} from "@ngrx/store";
import {people} from "../reducers/people";
import {partyFilter} from "../reducers/party-filter";
import {FilterSelect} from "./filter-select/filter-select.component";
import {PartyStats} from "./party-stats/party-stats.component";

@NgModule({
  declarations: [
    AppComponent,
    PersonInput,
    PersonList,
    FilterSelect,
    PartyStats
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [
    StoreModule.provideStore({people, partyFilter}).providers
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
