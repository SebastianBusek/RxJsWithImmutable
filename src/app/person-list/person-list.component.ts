import {
  Component,
  Input,
  EventEmitter,
  Output,
  ChangeDetectionStrategy
} from "@angular/core";

@Component({
  selector: 'person-list',
  styles: [`
    .mdl-checkbox {
      display: inline;
    }
  `],
  template: `
    <ul class="demo-list-three mdl-list">
      <li *ngFor="let person of people" class="mdl-list__item mdl-list__item--three-line">
        <span class="mdl-list__item-primary-content">
          <i class="material-icons mdl-list__item-avatar">person</i>
          <span>{{person.name}}</span>
          <span class="mdl-list__item-text-body">
            Guests: {{person.guests}}
            <button class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-js-ripple-effect" (click)="addGuest.emit(person.id)">
              <i class="material-icons">add</i>
            </button>
            <button class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-js-ripple-effect" (click)="removeGuest.emit(person.id)" [disabled]="!person.guests">
              <i class="material-icons">remove</i>
            </button>
            
            <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect">
              <input class="mdl-checkbox__input" type="checkbox" [checked]="person.attending" (change)="toggleAttending.emit(person.id)">
              <span class="mdl-checkbox__label">Attending</span>
            </label>
            
            <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-button--right" (click)="removePerson.emit(person.id)">Delete</button>
          </span>
        </span>
        <template [ngIf]="person.attending">
          <span class="mdl-list__item-secondary-content">
            <a class="mdl-list__item-secondary-action" href="#"><i class="material-icons">star</i></a>
          </span>
        </template>
      </li>
    </ul>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersonList {
  @Input() people;

  @Output() addGuest = new EventEmitter();
  @Output() removeGuest = new EventEmitter();
  @Output() removePerson = new EventEmitter();
  @Output() toggleAttending = new EventEmitter();
}
