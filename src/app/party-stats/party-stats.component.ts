import {Component, ChangeDetectionStrategy, Input} from "@angular/core";

@Component({
  selector: 'party-stats',
  template: `
    <p><strong>Invited:</strong> {{invited}}</p>
    <p><strong>Attending:</strong> {{attending}}</p>
    <p><strong>Guests:</strong> {{guests}}</p>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PartyStats {
  @Input() invited;
  @Input() attending;
  @Input() guests;
}
