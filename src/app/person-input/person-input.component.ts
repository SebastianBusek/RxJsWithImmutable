import {EventEmitter, Component, Output, ChangeDetectionStrategy} from "@angular/core";

@Component({
  selector: 'person-input',
  template: `
    <div>
      <strong>Invite person</strong>
    </div>
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
      <input class="mdl-textfield__input" #personName type="text" id="name">
      <label class="mdl-textfield__label" for="name">Name</label>
    </div>
    <button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored" style="float: right;" (click)="add(personName)">
      <i class="material-icons">add</i>
    </button>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersonInput {
  @Output() addPerson = new EventEmitter();

  add(personInput) {
    this.addPerson.emit(personInput.value);

    personInput.value = '';
  }
}
