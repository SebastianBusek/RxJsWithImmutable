import {SHOW_ALL, SHOW_ATTENDING, SHOW_WITH_GUESTS} from "../../actions/actions";
import {EventEmitter, Output, Component} from "@angular/core";

@Component({
  selector: 'filter-select',
  styles: [`
    .select-content {
      margin: 20px auto;
      text-align: center;
    }
    
    .select-list {
      padding: 10px;
    }
  `],
  template: `
    <div class="select-content">
      <select class="select-list" #selectList (change)="updateFilter.emit(selectList.value)">
        <option *ngFor="let filter of filters" value="{{filter.action}}">
          {{filter.friendly}}
        </option>
      </select>
    </div>
  `
})
export class FilterSelect {
  public filters = [
    {friendly: "All", action: SHOW_ALL},
    {friendly: "Attending", action: SHOW_ATTENDING},
    {friendly: "Attending w/ guests", action: SHOW_WITH_GUESTS}
  ];

  @Output() updateFilter: EventEmitter<string> = new EventEmitter<string>();
}
