///<reference path="../../node_modules/immutable/dist/immutable.d.ts"/>
import * as Immutable from 'immutable';

let personRecord = Immutable.Record({
  id: 0,
  name: '',
  attending: false,
  guests: 0
});

export class Person extends personRecord{
  id: number;
  name: string;
  attending: boolean;
  guests: number;

  constructor(id: number, name: string) {
    super({
      'id': id,
      'name': name
    });
  }

  set(key: string, value: any): Person {
    return <Person>super.set(key, value);
  }
}

