import {Action} from "@ngrx/store";
import {Person} from "../models/person";

export const ADD_PERSON = 'ADD_PERSON';
export const REMOVE_PERSON = 'REMOVE_PERSON';
export const ADD_GUEST = 'ADD_GUEST';
export const REMOVE_GUEST = 'REMOVE_GUEST';
export const TOGGLE_ATTENDING = 'TOGGLE_ATTENDING';

export const SHOW_ATTENDING = 'SHOW_ATTENDING';
export const SHOW_ALL = 'SHOW_ALL';
export const SHOW_WITH_GUESTS = 'SHOW_GUESTS';


export class AddPersonAction implements Action {
  type: string = ADD_PERSON;

  constructor(public payload: Person) {}
}

export class RemovePersonAction implements Action {
  type: string = REMOVE_PERSON;

  constructor(public payload: number) {}
}

export class RemoveGuestAction implements Action {
  type: string = REMOVE_GUEST;

  constructor(public payload: number) {}
}

export class AddGuestAction implements Action {
  type: string = ADD_GUEST;

  constructor(public payload: number) {}
}

export class ToggleAttendingAction implements Action {
  type: string = TOGGLE_ATTENDING;

  constructor(public payload: number) {}
}
