import {
  ADD_GUEST,
  REMOVE_GUEST,
  TOGGLE_ATTENDING,
  ADD_PERSON,
  REMOVE_PERSON
} from "../actions/actions";
import {Person} from "../models/person";
import {ActionReducer, Action} from "@ngrx/store";

const details : ActionReducer<Person> = (state: Person, action: Action) => {
  switch (action.type) {
    case ADD_GUEST:
      if (state.id === action.payload) {
        return state.set('guests', state.guests + 1);
      }
      return state;

    case REMOVE_GUEST:
      if (state.id === action.payload) {
        return state.set('guests', state.guests - 1);
      }
      return state;

    case TOGGLE_ATTENDING:
      if (state.id === action.payload) {
        return state.set('attending', !state.attending);
      }
      return state;

    default:
      return state;
  }
};

export const people : ActionReducer<Person[]> = (state: Person[] = [], action: Action) => {
  switch (action.type) {
    case ADD_PERSON:
      let person = <Person>action.payload;
      return [...state, person];

    case REMOVE_PERSON:
      return state.filter(person => person.id !== action.payload);

    case ADD_GUEST:
      return state.map(person => details(person, action));

    case REMOVE_GUEST:
      return state.map(person => details(person, action));

    case TOGGLE_ATTENDING:
      return state.map(person => details(person, action));

    default:
      return state;
  }
};
