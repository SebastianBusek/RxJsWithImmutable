import 'rxjs/Rx';
import {attendees} from "./attendees";

export const percentAttending = () => {
  return state => state.let(attendees()).map(p => {
    const attending = p.filter(person => person.attending).length;
    const total = p.length;

    return total > 0 ? (attending / total) * 100 : 0;
  });
};
