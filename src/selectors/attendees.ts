import 'rxjs/Rx';

export const attendees = () => {
  return state => state.map(s => s.people).distinctUntilChanged();
};
