import { StoreSamplePage } from './app.po';

describe('store-sample App', function() {
  let page: StoreSamplePage;

  beforeEach(() => {
    page = new StoreSamplePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
